package com.etna.userservice;

import com.etna.userservice.domain.User;
import com.etna.userservice.repository.UserRepository;
import com.etna.userservice.util.SkipOffsetPageable;
import com.etna.userservice.web.rest.UserController;
import org.apache.commons.text.RandomStringGenerator;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserServiceApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserRepository userRepository;

    @Test
    public void findAll_UsersFound_ShouldReturnFoundUsers() throws Exception {
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .build();
        User first = User.builder()
                .id(UUID.randomUUID())
                .firstName("Lorem ipsum")
                .lastName("Foo")
                .build();
        String firstName = generator.generate(20);
        String lastName = generator.generate(100);
        User second = User.builder()
                .id(UUID.randomUUID())
                .firstName(firstName)
                .lastName(lastName)
                .build();

        Mockito.when(
                userRepository.findAll(Mockito.any(Pageable.class))
        ).thenReturn(
                new PageImpl<User>(Arrays.asList(first, second))
        );

        mockMvc.perform(get("/devops-users/1.0.0/user"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].firstName", is("Lorem ipsum")))
                .andExpect(jsonPath("$[0].lastName", is("Foo")))
                .andExpect(jsonPath("$[1].firstName", is(firstName)))
                .andExpect(jsonPath("$[1].lastName", is(lastName)));

        Mockito.verify(userRepository, Mockito.times(1)).findAll(Mockito.any(Pageable.class));
        Mockito.verifyNoMoreInteractions(userRepository);
    }



    @Test
    public void findOne_UserFound_ShouldReturnFoundUser() throws Exception {
        RandomStringGenerator generator = new RandomStringGenerator.Builder()
                .build();
        UUID uuid = UUID.randomUUID();
        User first = User.builder()
                .id(uuid)
                .firstName("Lorem ipsum")
                .lastName("Foo")
                .build();

        Mockito.when(userRepository.findOne(uuid))
                .thenReturn(first);

        mockMvc.perform(get("/devops-users/1.0.0/user/{id}", uuid))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id", is(uuid.toString())))
                .andExpect(jsonPath("$.firstName", is("Lorem ipsum")))
                .andExpect(jsonPath("$.lastName", is("Foo")));

        Mockito.verify(userRepository, Mockito.times(1)).findOne(uuid);
        Mockito.verifyNoMoreInteractions(userRepository);
    }

}
