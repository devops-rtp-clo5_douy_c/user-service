package com.etna.userservice.util;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Created by chris on 16/08/2017.
 */
public class SkipOffsetPageable implements Pageable {

    private int limit = 0;
    private int offset = 0;

    public SkipOffsetPageable(Integer skip, Integer limit) {
        if (skip != null && skip < 0)
            throw new IllegalArgumentException("Skip must not be less than zero!");

        if (limit != null && limit < 0)
            throw new IllegalArgumentException("Offset must not be less than zero!");

        this.limit = limit != null ? limit : 0;
        this.offset = skip != null ? skip : 0;
    }

    @Override
    public int getPageNumber() {
        return 0;
    }

    @Override
    public int getPageSize() {
        return limit;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return null;
    }

    @Override
    public Pageable next() {
        return null;
    }

    @Override
    public Pageable previousOrFirst() {
        return this;
    }

    @Override
    public Pageable first() {
        return this;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }

}
