package com.etna.userservice.repository;

import com.etna.userservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by chris on 16/08/2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
}
