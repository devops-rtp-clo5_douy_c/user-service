package com.etna.userservice.web.rest;

import com.etna.userservice.domain.User;
import com.etna.userservice.repository.UserRepository;
import com.etna.userservice.util.SkipOffsetPageable;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


/**
 * Created by chris on 16/08/2017.
 */
@Controller
@RequestMapping("/devops-users/1.0.0")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value="/user", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers(@RequestParam(required = false) Integer skip, @RequestParam(required = false) Integer limit){
        Page<User> p = userRepository.findAll(new SkipOffsetPageable(skip, limit));
        return p != null ? p.getContent() : null;
       // return userRepository.findAll();
    }

    @RequestMapping(value="/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<User> getUser(@PathVariable UUID id){
        return new ResponseEntity<>(userRepository.findOne(id), HttpStatus.OK);
    }

    @RequestMapping(value="/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable UUID id, @RequestBody User user){
        User userDB = userRepository.findOne(id);
        if (userDB == null) return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        BeanUtils.copyProperties(user, userDB);
        return new ResponseEntity<>(userRepository.save(userDB), HttpStatus.OK);
    }

    @RequestMapping(value="/user", method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody User user){
        if (userRepository.findOne(Example.of(user)) != null)
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        return new ResponseEntity(userRepository.save(user), HttpStatus.CREATED);
    }
}
