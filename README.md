User microservice (First versions were made on https://gitlab.com/douy_c/user-service kept for the history, feel free to request access if needed)

A git push triggers the following :

1 -- Building the application (catch compile time errors asap). 

The compilation is done on a basic alpine image with Java8, and could also be done with the current image of the microservice

2 -- Building the Docker image (adding the source code and the .jar previously compiled on the image). 

This image will be pushed in the registry as a TEST_IMAGE, in order to avoid any impact on the current PROD image (aka latest)

The image currently only consists of an Alpine image with Java 8 installed.

3 -- Running the application tests on the Docker image (This ensures that both the code and the Docker Image are running just fine)

For this service we're doing a few unit tests, see inbox-service for integration tests examples

Theory would be to first run the unit tests (faster to execute, focusing on a single function) then the full integration tests (complex use cases/specific prod-like configuration). 

IF pushing on master : 

4 -- Pushing the previously created "TEST" image in the registry as "latest"

5 -- Deploying the application on Google Cloud with Kubernetes 

User service should be on http://35.195.241.229:8080/devops-user/1.0.0/ + route 

(see https://gitlab.com/devops-rtp-clo5_douy_c/user-service/environments otherwise, should have the most recent IP)